﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dev204x1
{
    class Program
    {
        static void Main(string[] args)
        {
            string studentfirstname;
            string studentlastname;
            DateTime studentbirthdate;
            string studentaddress1;
            string studentaddress2;
            string studentcity;
            string studentstateprovince;
            string studentzippostal;
            string studentcountry;
            int studentnumber;

            string professorfirstname;
            string professorlastname;
            DateTime professorbirthdate;
            string professoraddress1;
            string professoraddress2;
            string professorcity;
            string professorstateprovince;
            string professorzippostal;
            string professorcountry;

            string degreename;
            int creditsrequired;
            string prerequisite;

            string programname;
            string degreeoffered;
            string departmenthead;

            studentfirstname = "Jane";
            studentlastname = "Smith";
            studentbirthdate = new DateTime(1980, 1, 1);
            studentaddress1 = "1994 Richison Drive";
            studentaddress2 = string.Empty;
            studentcity = "Helena";
            studentstateprovince = "MT";
            studentzippostal = "59601";
            studentcountry = "United States";
            studentnumber = 9999;

            professorfirstname = "Nicholas";
            professorlastname = "Manuel";
            professorbirthdate = new DateTime(1960, 1, 1);
            professoraddress1 = "2877 Rosewood Lane";
            professoraddress2 = string.Empty;
            professorcity = "New York";
            professorstateprovince = "NY";
            professorzippostal = "10029";
            professorcountry = "United States";

            degreename = "Master of Fun";
            creditsrequired = 100;
            prerequisite = "none";

            programname = "Master of Fun";
            degreeoffered = "Masters";
            departmenthead = "Jane Doe";

            Console.WriteLine("student firstname: " + studentfirstname);
            Console.WriteLine("student lastname: " + studentlastname);
            Console.WriteLine("student birthdate: " + studentbirthdate);
            Console.WriteLine("student address1: " + studentaddress1);
            Console.WriteLine("student address2: " + studentaddress2);
            Console.WriteLine("student city: " + studentcity);
            Console.WriteLine("student state/province: " + studentstateprovince);
            Console.WriteLine("student zip/postal: " + studentzippostal);
            Console.WriteLine("student country: " + studentcountry);
            Console.WriteLine("student number: " + studentnumber);
            Console.WriteLine("professor firstname: " + professorfirstname);
            Console.WriteLine("professor lastname: " + professorlastname);
            Console.WriteLine("professor birthdate: " + professorbirthdate);
            Console.WriteLine("professor address1: " + professoraddress1);
            Console.WriteLine("professor address2: " + professoraddress2);
            Console.WriteLine("professor city: " + professorcity);
            Console.WriteLine("professor state/province: " + professorstateprovince);
            Console.WriteLine("professorcountry: " + professorcountry);
            Console.WriteLine("degree name: " + degreename);
            Console.WriteLine("credits required: " + creditsrequired);
            Console.WriteLine("prerequisite: " + prerequisite);
            Console.WriteLine("program name: " + programname);
            Console.WriteLine("degree offered: " + degreeoffered);
            Console.WriteLine("department head: " + departmenthead);
            Console.WriteLine("press any key to continue");
            Console.ReadKey();

        }
    }
}
